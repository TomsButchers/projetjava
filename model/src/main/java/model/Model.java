package model;

import java.sql.SQLException;

import java.util.Observable;

import contract.IModel;


/**
 * The Class Model.
 */
public class Model extends Observable implements IModel {

	/**	Monster ints	**/
/*	private int positionMonster1X;
	private int positionMonster1Y;
	private int positionMonster2X;
	private int positionMonster2Y;
	private int positionMonster3X;
	private int positionMonster3Y;
	private int positionMonster4X;
	private int positionMonster4Y;
	private int noMonster=1;
*/

	public static char[][] tabmap;
	private String lastPos = "LEFT";

	private int deathA = 0;
	private int deathB = 1;
	private int height = 12;
	private int width = 21;
	private int N = 0;
	/**   The hero    **/
	private Hero hero;
	/**   The score  **/
	private int score;

	public int getScore()
	{
		return this.score;
	}


	private int positionHeroX;
	private int positionHeroY;

	/**		Fireball **/
	public int positionShootY;
	public int positionShootX;
	public String fireDirection = "LEFT";
	private int noFire = 1;

	/**   Get and set Hero position  **/
	public int getPositionHeroX() {
		return positionHeroX;
	}

	public void setPositionHeroX(int positionHeroX) {
		this.positionHeroX = positionHeroX;
	}

	public int getPositionHeroY() {
		return positionHeroY;
	}

	public void setPositionHeroY(int positionHeroY) {
		this.positionHeroY = positionHeroY;
	}


	/** Get and Set monsters **/


	private MonsterA monsterA;

	public MonsterA getMonsterA(){
		return monsterA;
	}

	private MonsterB monsterB;

	public MonsterB getMonsterB(){
		return monsterB;
	}





	/**   Get et set la position des Portes  **/
	private int positionPorteX;
	private int positionPorteY;

	public int getPositionPorteX (){
		return positionPorteX;
	}

	public void setPositionPorteX(int positionPorteX) {
		this.positionPorteX = positionPorteX;
	}

	public int getPositionPorteY(){
		return positionPorteY;
	}

	public void setPositionPorteY(int positionPorteY){
		this.positionPorteY = positionPorteY;

	}


	/**   Get size of the grid **/

	public int getHeight(){
		return this.height;
	}

	public int getWidth() {
		return this.width;
	}

	public char[][] tabmap2d = new char[this.getHeight()][this.getWidth()];

	public void setScore(int score)
	{
		this.score = score;

	}

	/** The message. */
	private String message;

	/**
	 * Instantiates a new model.
	 */
	public Model() {
		this.message = "";
		this.hero = new Hero();
		this.monsterA = new MonsterA();
		this.monsterB = new MonsterB();
	}



	/** Get Hero */
	public Hero getHero(){
		return this.hero;
	}

	public void getMapInChar() {

	}
	/**   Get the Message**/
	public String getMessage() {
		return this.message;

	}

	/** Creation of the Char Table**/
	public void putInTabmap(int i, int j, char car) {
		this.tabmap2d[i][j] = car;
	}

	/**   Creation of the Parser table**/
	public void doTheThing() {
		String[] tabmap = this.message.split("\n") ;
		this.setScore(0);

		for(int i =0; i<tabmap.length; i++)
		{
			for (int j =0; j<tabmap[i].length();j++)
			{
				switch (tabmap[i].charAt(j)) {
					case'O':
						this.putInTabmap(i,j,'O');
						break;
					case'R':
						this.putInTabmap(i,j,'R');
						break;
					case'S':
						this.putInTabmap(i,j,'S');
						break;
					case'T':
						this.putInTabmap(i,j,'T');
						break;
					case'Q':
						this.putInTabmap(i,j,'Q');
						positionPorteX=j;
						positionPorteY=i;

						break;
					case'P':
						this.putInTabmap(i,j,'P');
						setPositionPorteX(j);
						setPositionPorteY(i);

						break;
					case'N':
						this.putInTabmap(i,j,'N');

						break;
					case'M':
						this.putInTabmap(i,j,'M');
						break;
					case'L':
						this.putInTabmap(i,j,'L');
						break;
					case'K':
						this.putInTabmap(i,j,'K');
						break;
					case'J':
						this.putInTabmap(i,j,'J');
						break;
					case'I':
						this.putInTabmap(i,j,'I');
						break;
					case'U':
						this.putInTabmap(i,j,'U');
						this.getMonsterA().setX(4);
						this.getMonsterA().setY(i);
						System.out.println("Y : "+i);
						System.out.println("X : "+this.getMonsterA().getX());
						break;
					case'V':
						this.putInTabmap(i,j,'V');
						this.getMonsterB().setX(j);
						this.getMonsterB().setY(i);
						break;
					case'X':
						this.putInTabmap(i,j,'X');
						break;
					case'Y':
						this.putInTabmap(i,j,'Y');
						break;
					case'Z':
						this.putInTabmap(i,j,'Z');
						break;
					case'B':
						this.putInTabmap(i,j,'B');
						break;
					case'C':
						this.putInTabmap(i,j,'C');
						break;
					case'D':
						this.putInTabmap(i,j,'D');
						break;
					case'E':
						this.putInTabmap(i,j,'E');
						break;
					case'F':
						this.putInTabmap(i,j,'F');
						break;
					case'A':
						this.putInTabmap(i,j,'A');
						setPositionHeroX(j);
						setPositionHeroY(i);
						break;
				}
			}
		}
		
	}
	public char[][] getTabmap2d() {
		return this.tabmap2d;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *          the new message
	 */
	private void setMessage(final String message) {
		this.message = message;
		this.setChanged();
		this.notifyObservers();
	}

	/** load Message  **/
	public void loadMessage(final String key) {
		try {
			final DAOHelloWorld daoHelloWorld = new DAOHelloWorld(DBConnection.getInstance().getConnection());
			this.setMessage(daoHelloWorld.find(key).getMessage());
			this.deathA = 0;
			//this.deathB = 0;

			} catch (final SQLException e) {
			e.printStackTrace();
		}
	}

	/** Get position for the creation of the fireball	**/
	public int getPositionShootY() {
		return positionShootY;
	}

	public void setPositionShootY(int positionShootY) {
		this.positionShootY = positionShootY;
	}

   public  boolean MisMovePossible(int y, int x){
	   return (tabmap2d[y][x]== 'O'|| tabmap2d[y][x]=='A');
   }

	public void moveLeft(int x, int y) {
		if (isMovePossible(x, y) == true) {
			Move.Move(positionHeroY, positionHeroX, y, x, 'S');
		} else {
		}
	}

    public void setLevel(int level) {

    }

    public int getLevel() {
        return 0;
    }

    /** Void for the fireball	**/
	/*public void fire() {
		if (noFire == 1) {
			noFire = 0;
			//System.out.println(positionHeroX);
			//System.out.println(positionHeroY);

			if (lastPos == "LEFT") {
				tabmap2d[positionHeroY][positionHeroX-1] = 'I';
				fireDirection = "LEFT";
			}
		}


	}
*/
    public void fire(){
        if (noFire==1){ //Check if the fire ball is available
            noFire=0; // Put the fireball in "unavailable" mode
            if (lastPos=="LEFT"){ // Wheck the hero last position in order to create the fireball in the corresponding direction.
                if(fMovePossible(this.positionHeroX-1, this.positionHeroY)) // Check if the fireball can be created (here, on the left).
                {
                    setPositionShootX(positionHeroX-1); // Save the last position of the ball.
                    setPositionShootY(positionHeroY);
                    tabmap2d[positionShootY][positionShootX]='I'; // Put the W symbol (fireball) in the array
                    fireDirection="LEFT"; //Put the direction of the fireball to left.
                }
            }
            else if (lastPos=="RIGHT"){ // Same as first, with Right instead of Left
                if(fMovePossible(this.positionHeroX+1, this.positionHeroY))
                {
                    setPositionShootX(positionHeroX+1);
                    setPositionShootY(positionHeroY);
                    tabmap2d[positionShootY][positionShootX]='I';
                    fireDirection="RIGHT";
                }
            }

            else if (lastPos=="UP"){ // Same with UP
                if(fMovePossible(this.positionHeroX, this.positionHeroY-1))
                {
                    setPositionShootX(positionHeroX);
                    setPositionShootY(positionHeroY-1);
                    tabmap2d[positionShootY][positionShootX]='I';
                    fireDirection="UP";
                }
            }

            else if (lastPos=="DOWN"){ // Same with Down
                if(fMovePossible(this.positionHeroX, this.positionHeroY+1))
                {
                    setPositionShootX(positionHeroX);
                    setPositionShootY(positionHeroY+1);
                    tabmap2d[positionShootY][positionShootX]='I';
                    fireDirection="DOWN";
                }
            }
        }
    }
	public void setLastPos(String LastPos) {
        this.lastPos = LastPos;	}

	/** This void is making the fireball moving	**/
    public void fireAnimation(){
        if (noFire==0){ // If there is no fireball available, then the fireball present on the map can move.
            if (fireDirection=="LEFT"){// If the last position is Left, then the ball will move to the left.

                if(fMovePossible(this.positionShootX+(-1), this.positionShootY))// Verifying that there is no obstacle
                {
                    tabmap2d[positionShootY][positionShootX]='O';// Its position is replaced by void
                    setPositionShootX(this.positionShootX-1);// Saving the new position
                    tabmap2d[positionShootY][positionShootX]='I';// Showing the ball at the new position.
                    setPositionShootY(this.positionShootY);
                }
                else {	// If the ball meet an obstacle, it bounce
                    tabmap2d[positionShootY][positionShootX]='O';
                    setPositionShootX(this.positionShootX+1);// Incrementing it's position opposite to where it was moving
                    setPositionShootY(this.positionShootY);
                    tabmap2d[positionShootY][positionShootX]='I';
                    fireDirection="RIGHT";
                }
            }
            if (fireDirection=="RIGHT"){ // Same but right
                if(fMovePossible(this.positionShootX+1, this.positionShootY))
                {
                    tabmap2d[positionShootY][positionShootX]='O';
                    setPositionShootX(this.positionShootX+1);
                    setPositionShootY(this.positionShootY);
                    tabmap2d[positionShootY][positionShootX]='I';

                }
                else {
                    tabmap2d[positionShootY][positionShootX]='O';
                    setPositionShootX(this.positionShootX-1);
                    tabmap2d[positionShootY][positionShootX]='I';
                    setPositionShootY(this.positionShootY);
                    fireDirection="LEFT";
                }
            }

            // Same
            if (fireDirection=="UP") if (fMovePossible(this.positionShootX, this.positionShootY - 1)) {
                tabmap2d[positionShootY][positionShootX] = 'O';
                setPositionShootX(this.positionShootX);
                setPositionShootY(this.positionShootY - 1);
                tabmap2d[positionShootY][positionShootX] = 'I';

            } else {
                tabmap2d[positionShootY][positionShootX] = 'O';
                setPositionShootX(this.positionShootX);
                setPositionShootY(this.positionShootY + 1);
                tabmap2d[positionShootY][positionShootX] = 'I';
                fireDirection = "DOWN";
            }

            if (fireDirection=="DOWN"){ // same
                if(fMovePossible(this.positionShootX, this.positionShootY+1))
                {
                    tabmap2d[positionShootY][positionShootX]='O';
                    setPositionShootX(this.positionShootX);
                    setPositionShootY(this.positionShootY+1);
                    tabmap2d[positionShootY][positionShootX]='I';
                }
                else {
                    tabmap2d[positionShootY][positionShootX]='O';
                    setPositionShootX(this.positionShootX);
                    setPositionShootY(this.positionShootY-1);
                    tabmap2d[positionShootY][positionShootX]='I';
                    fireDirection="UP";
                }
            }
        }
    }

	public void monsterAMove() {

	}

	public boolean checkPosition(){
		if(positionHeroX == monsterA.getX() && positionHeroY == monsterA.getY()){
			return true;
		}
		return false;
	}

	public Observable getObservable() {
		return this;
	}

	/** Move Hero **/

	public void moveG(int x ,int y) {
		if(isMovePossible(positionHeroX+x, positionHeroY+y))
		{

			tabmap2d[positionHeroY+y][positionHeroX+x]='A';
			tabmap2d[positionHeroY][positionHeroX]='O';
		    setPositionHeroX(positionHeroX+x);
			setPositionHeroY(positionHeroY+y);

					}

		else {}
	}

    /** 	Void to see if the fireball's moving is possible	**/
	public boolean fMovePossible(int x, int y){
		System.out.println(x);
		return (this.tabmap2d[y][x]!='S'
				&& this.tabmap2d[y][x]!='N'&& this.tabmap2d[y][x]!='T'
				&& this.tabmap2d[y][x]!='Q'&& this.tabmap2d[y][x]!='Z'
				&& this.tabmap2d[y][x]!='R');
	}

    public void fireCheck(){ ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if(positionShootY == positionHeroY && positionShootX == positionHeroX){
            noFire = 1 ;
            tabmap2d[positionHeroY][positionHeroX]='A';

        }
        else if(positionShootY == monsterA.getY() && positionShootX == monsterA.getX() ){
            this.deathA = 1;
            tabmap2d[positionShootY][positionShootX]='O';
        }
		else if(positionShootY == monsterB.getY() && positionShootX == monsterB.getX() ){
			this.deathB =1;
			tabmap2d[positionShootY][positionShootX]='O';
		}


    }

	/**   Position of the door **/
	public void Door(int x, int y){
		if(isMovePossible(positionPorteX+x, positionPorteY+y)){


			tabmap2d[positionHeroY+y][positionHeroX+x]='Q';
			tabmap2d[positionPorteY+y][positionPorteX+x]='P';
			tabmap2d[positionHeroY+y][positionHeroX+x]='U';

		}
		else{}
	}

	public boolean isMovePossible(int x,  int y)
	{

		if(tabmap2d[y][x] == 'O'){
			return true;
		}
		if(tabmap2d[y][x] == 'Z'){
			this.setScore(this.getScore()+100);
			return true;
		}
		if(tabmap2d[y][x] == 'N'){
			tabmap2d[positionPorteY] [positionPorteX] = 'P';
			return true;
		}
		if(tabmap2d[y][x] == 'P'){
			this.loadMessage("m0");
			return true;
		}

		return  false;
	}


	/** Void monster	**/

/*	public void moveMonster1(){
		int minimum=-1;
		int maximum=3;
		int randomNumX = minimum + (int)(Math.random() * maximum);
		int randomNumY = minimum + (int)(Math.random() * maximum);

		if(isMoveMonsterPossible(positionMonster1X,positionMonster1Y,randomNumX, randomNumY) == true && noMonster==1)
		{
			Move1.Move(positionMonster1Y,positionMonster1X,randomNumY,randomNumX,'1');
			setPositionMonster1X(positionMonster1X+randomNumX);
			setPositionMonster1Y(positionMonster1Y+randomNumY);
		}
	}

	public void moveMonster2(){
		int minimum=-1;
		int maximum=3;
		int randomNumX = minimum + (int)(Math.random() * maximum);
		int randomNumY = minimum + (int)(Math.random() * maximum);

		if(isMoveMonsterPossible(positionMonster2X,positionMonster2Y,randomNumX, randomNumY) == true)
		{
			Move1.Move(positionMonster2Y,positionMonster2X,randomNumY,randomNumX,'2');
			setPositionMonster2X(positionMonster2X+randomNumX);
			setPositionMonster2Y(positionMonster2Y+randomNumY);
		}
	}

	public void moveMonster3(){
		int minimum=-1;
		int maximum=3;
		int randomNumX = minimum + (int)(Math.random() * maximum);
		int randomNumY = minimum + (int)(Math.random() * maximum);
		if(isMoveMonsterPossible(positionMonster3X,positionMonster3Y,randomNumX, randomNumY) == true)
		{
			Move1.Move(positionMonster3Y,positionMonster3X,randomNumY,randomNumX,'3');
			setPositionMonster3X(positionMonster3X+randomNumX);
			setPositionMonster3Y(positionMonster3Y+randomNumY);
		}
	}

	public void moveMonster4(){
		int minimum=-1;
		int maximum=3;
		int randomNumX = minimum + (int)(Math.random() * maximum);
		int randomNumY = minimum + (int)(Math.random() * maximum);

		if (isMoveMonsterPossible(positionMonster4X,positionMonster4Y,randomNumX, randomNumY) == true){
			Move1.Move(positionMonster4Y,positionMonster4X,randomNumY,randomNumX,'2');
			setPositionMonster4X(positionMonster4X+randomNumX);
			setPositionMonster4Y(positionMonster4Y+randomNumY);
		}
	}

	public boolean isMoveMonsterPossible(int posx, int posy, int x,  int y)
	{

		switch (tabmap2d[posy+(y)][posx+(x)]){
			case 'O':
				return true;
			case'A':
				gameOver(score);
				return true;
			default:
				return false;
		}
	}

    private void gameOver(int score) {
        //SAVE SCORE
        this.setLevel(100);
        this.loadMessage("7");
    }

*/




    	public void monsterA() {
        //  if (this.getO() == 3) {

            int mx = getMonsterA().getX();
            int hx = getPositionHeroX();
            int my = getMonsterA().getY();
            int hy = getPositionHeroY();

			/*System.out.println(this.positionShootX);
			System.out.println(this.positionShootY);*/

			if(deathA == 0)
			{
				if (mx < hx && MisMovePossible(getMonsterA().getY(),getMonsterA().getX() + 1) ){
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'O';
					this.getMonsterA().setX(this.getMonsterA().getX()+1);
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'U';
				}
				/** if x is higher than lorann's decrement it**/
				if (mx > hx && MisMovePossible(getMonsterA().getY(),getMonsterA().getX() -1)) {
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'O';
					this.getMonsterA().setX(this.getMonsterA().getX()-1);
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'U';
				}
				/** if y is lower than lorann's increments it**/
				if (my < hy && MisMovePossible(getMonsterA().getY(),getMonsterA().getX() + 1)){
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'O';
					this.getMonsterA().setY(this.getMonsterA().getY()+1);
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'U';
				}
				/** if y is higher than lorann's decrement it **/
				if (my > hy && MisMovePossible(getMonsterA().getY(),getMonsterA().getX() - 1 ) ) {
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'O';
					this.getMonsterA().setY(this.getMonsterA().getY()-1);
					this.tabmap2d[this.getMonsterA().getY()][this.getMonsterA().getX()] = 'U';
			}
		}
	}


	public void monsterB() {
		//  if (this.getO() == 3) {

			int mx = getmonsterB().getX();
			int hx = getPositionHeroX();
			int my = getmonsterB().getY();
			int hy = getPositionHeroY();

			if(this.deathB == 0)
			{

			/** if x is lower than lorann's increments it**/
			if (mx < hx && MisMovePossible(getmonsterB().getY(),getmonsterB().getX() + 1) ){
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'O';
				this.getmonsterB().setX(this.getmonsterB().getX()+1);
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'V';
			}
			/** if x is higher than lorann's decrement it**/
			if (mx > hx && MisMovePossible(getmonsterB().getY(),getmonsterB().getX() -1)) {
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'O';
				this.getmonsterB().setX(this.getmonsterB().getX()-1);
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'V';
			}
			/** if y is lower than lorann's increments it**/
			if (my < hy && MisMovePossible(getmonsterB().getY(),getmonsterB().getX() + 1)){
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'O';
				this.getmonsterB().setY(this.getmonsterB().getY()+1);
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'V';
			}
			/** if y is higher than lorann's decrement it **/
			if (my > hy && MisMovePossible(getmonsterB().getY(),getmonsterB().getX() - 1 ) ) {
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'O';
				this.getmonsterB().setY(this.getmonsterB().getY()-1);
				this.tabmap2d[this.getmonsterB().getY()][this.getmonsterB().getX()] = 'V';
			}
		}
	}




	public void setPositionShootX(int positionShootX) {
		this.positionShootX = positionShootX;
	}

	public MonsterB getmonsterB() {
		return monsterB;
	}


/*
	public void setPositionMonster1X(int positionMonster1X) {
		this.positionMonster1X = positionMonster1X;
	}

	public void setPositionMonster1Y(int positionMonster1Y) {
		this.positionMonster1Y = positionMonster1Y;
	}

	public void setPositionMonster2X(int positionMonster2X) {
		this.positionMonster2X = positionMonster2X;
	}

	public void setPositionMonster2Y(int positionMonster2Y) {
		this.positionMonster2Y = positionMonster2Y;
	}

	public void setPositionMonster3X(int positionMonster3X) {
		this.positionMonster3X = positionMonster3X;
	}

	public void setPositionMonster3Y(int positionMonster3Y) {
		this.positionMonster3Y = positionMonster3Y;
	}

	public void setPositionMonster4X(int positionMonster4X) {
		this.positionMonster4X = positionMonster4X;
	}

	public void setPositionMonster4Y(int positionMonster4Y) {
		this.positionMonster4Y = positionMonster4Y;
	}
*/
    
}




