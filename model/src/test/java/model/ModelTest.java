/**
 *
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * The Class ModelTest.
 *
 * @author Jean-Aymeric Diet
 */
public class ModelTest {
	@Test
	public void getScore() throws Exception {

	}

	@Test
	public void getPositionHeroX() throws Exception {

	}

	@Test
	public void setPositionHeroX() throws Exception {

	}

	@Test
	public void getPositionHeroY() throws Exception {

	}

	@Test
	public void setPositionHeroY() throws Exception {

	}

	@Test
	public void getMonsterA() throws Exception {

	}

	@Test
	public void getPositionPorteX() throws Exception {

	}

	@Test
	public void setPositionPorteX() throws Exception {

	}

	@Test
	public void getPositionPorteY() throws Exception {

	}

	@Test
	public void setPositionPorteY() throws Exception {

	}

	@Test
	public void monsterA() throws Exception {

	}

	@Test
	public void getHeight() throws Exception {

	}

	@Test
	public void getWidth() throws Exception {

	}

	@Test
	public void setScore() throws Exception {

	}

	@Test
	public void getHero() throws Exception {

	}

	@Test
	public void getMapInChar() throws Exception {

	}

	@Test
	public void getMessage() throws Exception {

	}

	@Test
	public void putInTabmap() throws Exception {

	}

	@Test
	public void doTheThing() throws Exception {

	}

	@Test
	public void getTabmap2d() throws Exception {

	}

	@Test
	public void loadMessage() throws Exception {

	}

	@Test
	public void getPositionShootY() throws Exception {

	}

	@Test
	public void setPositionShootY() throws Exception {

	}

	@Test
	public void moveLeft() throws Exception {

	}

	@Test
	public void setLevel() throws Exception {

	}

	@Test
	public void getLevel() throws Exception {

	}

	@Test
	public void fire() throws Exception {

	}

	@Test
	public void setLastPos() throws Exception {

	}

	@Test
	public void fireAnimation() throws Exception {

	}

	@Test
	public void getObservable() throws Exception {

	}

	@Test
	public void moveG() throws Exception {

	}

	@Test
	public void fMovePossible() throws Exception {

	}

	@Test
	public void door() throws Exception {

	}

	@Test
	public void isMovePossible() throws Exception {

	}

	@Test
	public void monsterA1() throws Exception {

	}

	@Test
	public void getPositionMonstersX() throws Exception {

	}

	@Test
	public void setPositionMonstersX() throws Exception {

	}

	@Test
	public void getPositionMonsersY() throws Exception {

	}

	@Test
	public void setPositionMonstersY() throws Exception {

	}

	@Test
	public void setPositionShootX() throws Exception {

	}

	private Model model;

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception
	 *           the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Tear down after class.
	 *
	 * @throws Exception
	 *           the exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *           the exception
	 */
	@Before
	public void setUp() throws Exception {
		this.model = new Model();
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *           the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link model.Model#getMessage()}.
	 */
	@Test
	public void testGetMessage() {
		Assert.assertEquals("", this.model.getMessage());
	}

	/**
	 * Test method for {@link model.Model#loadMessage(java.lang.String)}.
	 */
	@Test
	public void testGetMessageString() {
		this.model.loadMessage("GB");
		Assert.assertEquals("Hello world", this.model.getMessage());
		this.model.loadMessage("FR");
		Assert.assertEquals("Bonjour le monde", this.model.getMessage());
		this.model.loadMessage("DE");
		Assert.assertEquals("Hallo Welt", this.model.getMessage());
		this.model.loadMessage("ID");
		Assert.assertEquals("Salamat pagi dunia", this.model.getMessage());
	}

}
