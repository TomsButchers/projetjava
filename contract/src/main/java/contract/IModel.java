package contract;

import java.util.Observable;



/**
 * The Interface IModel.
 */
public interface IModel {

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	String getMessage();

	/**
	 * Load the message.
	 *
	 * @param key
	 *          the key
	 */
	void loadMessage(String key);

	void doTheThing();

	/** Get Score **/
	int getScore();

	/** Call the parser**/
	void putInTabmap(int i, int j, char car);

  	/** Get and Set position hero and Door**/
	char[][] getTabmap2d();
	

	boolean checkPosition();

	void Door(int x , int Y);

	int getPositionPorteX();

	void setPositionPorteX(int positionPorteX);

	int getPositionPorteY();

	void setPositionPorteY(int positionPorteY);


	/** GET AND SET POSOTION OF MONSTERS **/
	void monsterA();
	void monsterB();


	/**
	 * Gets the observable.
	 *
	 * @return the observable
	 */
	Observable getObservable();


	void moveG(int x , int y);

	void fire();

	void setLastPos(String LastPos);

	void fireAnimation();

	public void fireCheck();


	void monsterAMove();
}
