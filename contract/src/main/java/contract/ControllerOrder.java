package contract;

/**
 * The Enum ControllerOrder.
 */
public enum ControllerOrder {

	m1, /** The map 1*/
	m2, /** The map 2 */
	m3, /** The map 3 */
	m4, /** The map 4 */
	m5,  /** The map 5 */
	m0, /** The map 0 */
	MoveLeft, /** The Key Left  **/
	MoveRight, /** The Key Right **/
	MoveUp,/** The Key Up  **/
	MoveDown,/** The Key Down **/
	Fire, /** The key to launch a fireball **/
	//DiaLeftUp;
}
