package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JPanel;



/**
 * The Class ViewPanel.
 */
class ViewPanel extends JPanel implements Observer {

	/** The view frame. */
	private ViewFrame					viewFrame;
	/** The Constant serialVersionUID. */
	private static final long	serialVersionUID	= -998294702363713521L;
	char[][] tabmap ;
	/**
	 * Instantiates a new view panel.
	 *
	 * @param viewFrame
	 *          the view frame
	 */
	public ViewPanel(final ViewFrame viewFrame) {
		this.setViewFrame(viewFrame);
		viewFrame.getModel().getObservable().addObserver(this);
	}

	/**
	 * Gets the view frame.
	 *
	 * @return the view frame
	 */
	private ViewFrame getViewFrame() {
		return this.viewFrame;
	}

	/**
	 * Sets the view frame.
	 *
	 * @param viewFrame
	 *          the new view frame
	 */
	private void setViewFrame(final ViewFrame viewFrame) {
		this.viewFrame = viewFrame;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(final Observable arg0, final Object arg1) {
		this.viewFrame.getModel().doTheThing();
		this.repaint();
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	
	@Override
	protected void paintComponent(final Graphics graphics) {
		graphics.clearRect(0, 0, this.getWidth(), this.getHeight());
		graphics.setColor(Color.BLACK);
		graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
		this.viewFrame.getModel().monsterA();
		this.viewFrame.getModel().monsterB();

		if(this.viewFrame.getModel().checkPosition()){
			this.viewFrame.getModel().loadMessage("m0");
		}

		int score= this.viewFrame.getModel().getScore();

		Font fonte = new Font("Courier New",Font.BOLD,25);
		graphics.setFont(fonte);
		graphics.setColor(Color.yellow);
		graphics.drawString("Map Score : " + this.viewFrame.getModel().getScore(),400,415);

		this.viewFrame.getModel().fireCheck();
		this.viewFrame.getModel().fireAnimation();
		this.viewFrame.getModel().fireCheck();
		//this.viewFrame.getModel().moveLeft();
		afficherMap(graphics);

		if(this.viewFrame.getModel().checkPosition()){
			this.viewFrame.getModel().loadMessage("m0");
		}

	}
	
	public void afficherMap(final Graphics graphics) {
		
		this.tabmap = this.viewFrame.getModel().getTabmap2d();

		for(int i =0; i<this.tabmap.length; i++)
		{
			for (int j =0; j<this.tabmap[i].length;j++)
			{
				switch (this.tabmap[i][j]) {
					case 'Z':try {
						Image img = ImageIO.read(new File("sprite/purse.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;
					case 'R':try {

						Image img = ImageIO.read(new File("sprite/bone.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'S':try {
						Image img = ImageIO.read(new File("sprite/horizontal_bone.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'T':try {
						Image img = ImageIO.read(new File("sprite/vertical_bone.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'Q':try {
						Image img = ImageIO.read(new File("sprite/gate_closed.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;


					case 'P':try {
						Image img = ImageIO.read(new File("sprite/gate_open.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'N':try {
						Image img = ImageIO.read(new File("sprite/crystal_ball.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'I':
						graphics.drawImage(new ImageIcon("sprite/fireball_animated.gif").getImage(), 32*j, 32*i, this);

					break;

					case 'U':try {
						Image img = ImageIO.read(new File("sprite/monster_1.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'V':try {
						Image img = ImageIO.read(new File("sprite/monster_2.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'X':try {
						Image img = ImageIO.read(new File("sprite/monster_3.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'B':try {
						Image img = ImageIO.read(new File("sprite/Sprite1.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'C':try {
						Image img = ImageIO.read(new File("sprite/Sprite2.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'D':try {
						Image img = ImageIO.read(new File("sprite/Sprite3.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'E':try {
						Image img = ImageIO.read(new File("sprite/Sprite4.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;

					case 'F':try {
						Image img = ImageIO.read(new File("sprite/Sprite5.png"));
						graphics.drawImage(img, 32*j, 32*i, this);
					} catch(IOException e) {
						e.printStackTrace();
					}
						break;


					case 'A':
						graphics.drawImage(new ImageIcon("sprite/lorann.gif").getImage(), 32*j, 32*i, this);
						break;




						
						
				}
				
			}

		}
		
	}
}
